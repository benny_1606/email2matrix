# Email2Matrix

A simple application that relay email to [Matrix](https://matrix.org/).

# Requirements

* For any method:
	* [libolm](https://gitlab.matrix.org/matrix-org/olm) C library (version 3.X), see https://github.com/poljar/matrix-nio/blob/master/README.md#installation, i.e.
		* Debain and Ubuntu: `apt install libolm-dev`
		* Fedora: use `dnf` to install package `libolm-devel`
		* MacOS: use `brew` to install package `libolm`, version 3 required

* For using release:
	- Linux x86_64

* For installing from source:
	* Python3.8+ (3.7 or below will NOT work)
	* Only Arch Linux x86_64 tested, should work on other OS

# Installation

1. Download this program, by one of the following options:
	1. `git clone --depth=1 https://gitlab.com/end_my_suffering/email2matrix`
	and `git submodule update --init --recursive`
	2.  OR from release (Extract archive to a **single dedicated** directory, skip to step 3)
	
2. `pip3 install --user -r requirements.txt `(or without `--user` to install python packages system-wise)
3. `cd matrix-commander`, then execute matrix-commander by`python3 matrix-commander.py ` (For release method, simply `./matrix-commander`) to configure Matrix client.
4. `cd ..; cp config.sample.ini config.ini` (For release method, `cp config.sample.ini config.ini`)
5. Edit `config.ini`

6. Optional, if one wishes to use systemd:

```
useradd -m -b /var/lib recv_noti_bot
cp etc/systemd/system/email2matrix.service /etc/systemd/system/
systemctl enable email2matrix
systemctl start email2matrix
```

# Usage

* If you download using `git clone`,

`python3 main.py`

* If you download using release,

`./email2matrix` 

# License

    Email2Matrix
    Copyright (C) 2020  Adam S

# Acknowledgement

The following FOSS software are used.

* [matrix-commander](https://github.com/8go/matrix-commander). GPLv3. 
[![Built with matrix-nio](
https://img.shields.io/badge/built%20with-matrix--nio-brightgreen)](
https://github.com/poljar/matrix-nio)
* [Tenacity](https://github.com/jd/tenacity). Apache License 2.0