#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd $DIR

PATH=$WORKSPACE/.env/bin:/usr/local/bin:$PATH

if [[ ! -d ".env" ]]; then
	python3 -m venv .env
fi

source .env/bin/activate

pip3 install wheel
pip3 install -r requirements.txt
pip3 install pyinstaller

pyinstaller -y -F -p email2matrix email2matrix/main.py
pyinstaller -y -D -p email2matrix/matrix-commander email2matrix/matrix-commander/matrix-commander.py

cp email2matrix/config.sample.ini dist/

cd dist
mv main email2matrix
rm -f *.tar
rm -f *.tar.zst
tar --zstd -cf "email2matrix_Linux_x86_64_$(git describe --long).tar.zst" email2matrix matrix-commander config.sample.ini
