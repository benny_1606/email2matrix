#!/bin/bash

<<COMMENT
Directory tree should be,

matrix
├── credentials.json
├── matrix_alert.sh
├── matrix-commander
└── store

---
# Usage

## SYNOPSIS

```
matrix_alert.sh TO SUBJECT BODY
```

## DESCRIPTION

Send a Matrix markdown message with SUBJECT and BODY.

TO does nothing other than a placeholder, needed for Zabbix.

COMMENT


to=$1
subject=$2
body=$3

read -r -d '' msg << EOF
---
\`From matrix_commander executed by Zabbix\`

\`Subject\`: $subject


$body
EOF

cd "$( dirname "${BASH_SOURCE[0]}" )"
./matrix-commander -zm "$msg"

exit 0