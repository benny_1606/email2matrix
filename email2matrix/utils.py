import sys
import pathlib


def is_bundled() -> bool:
    if getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS'):
        return True
    else:
        return False


def current_path():
    if is_bundled():
        path = pathlib.Path(sys.executable).parent.resolve()
    else:
        path = pathlib.Path(__file__).parent.resolve()

    return path
