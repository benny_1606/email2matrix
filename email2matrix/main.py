#!/usr/bin/env python3

import ssl
import time
import logging
import configparser
import concurrent.futures
import queue
import threading
import traceback
import pathlib
import utils
from sys import exit as sys_exit
from os import _exit as os_exit
from imapclient import IMAPClient, SocketTimeout
from fetch_email import fetch_email
from run_matrix import send_to_matrix
from tenacity import retry, wait_exponential, retry_if_exception_type
from socket import timeout as socket_timeout


@retry(retry=(retry_if_exception_type(BrokenPipeError)
              | retry_if_exception_type(socket_timeout)
              | retry_if_exception_type(OSError)),
       wait=wait_exponential(multiplier=1, min=5, max=30))
def main(matrix_queue, exit_event):

    ssl_context = ssl.create_default_context()
    imap_timeout = SocketTimeout(connect=5, read=300)

    logging.info('Starting')
    logging.info(f"Connecting to {config['IMAP4 server']['HOST']}")
    server = IMAPClient(config['IMAP4 server']['HOST'], ssl_context=ssl_context, use_uid=True,
                        timeout=imap_timeout)

    logging.info(f"Logging in as {config['IMAP4 server']['USERNAME']}")
    server.login(config['IMAP4 server']['USERNAME'],
                 config['IMAP4 server']['PASSWORD'])

    logging.info('Select folder: INBOX')
    server.select_folder('INBOX')

    msg_to_send = list()
    new_msg_count = len(server.sort('ARRIVAL'))

    if new_msg_count > 0:
        new_msg = fetch_email(server, config)
        new_msg.insert(
            0,
            f"## {new_msg_count} email(s) found when booting up.\n"
        )
        logging.debug(f"new_msg: {new_msg}")
        matrix_queue.put(new_msg)

    logging.info('Finished bootup. Entering IDLE')

    # Putting server to IDLE
    server.idle()

    init_time = time.monotonic()
    last_time = None

    while not exit_event.is_set():
        time_now = time.monotonic()
        if last_time is None:
            last_time = time_now

        elif last_time is not None and last_time + 0.05 > time_now:
            logging.error('Server maybe down, infinite loop occured')
            raise BrokenPipeError(
                "Server maybe down, infinite loop occured.")

        elif time_now >= (init_time + float(config['IMAP4 server']['RECON_INTVL'])):
            # Renew IDLE command, as recommended by IMAPClient
            logging.debug('Renewing IDLE command')
            server.idle_done()
            server.idle()
            init_time = time_now

        last_time = time_now
        responses = server.idle_check(timeout=5)

        if responses \
                and len(responses) == 2 \
                and responses[0][1] == b'EXISTS':
            logging.info('New email detected, fetching')
            server.idle_done()
            msg_to_send = fetch_email(server, config)
            logging.debug(f"msg_to_send: {msg_to_send}")

            matrix_queue.put(msg_to_send)

            server.idle()

    server.idle_done()
    server.logout()


def shutdown(futs, exception=None):
    """
    Gracefully shutdown all futs, wait for 40s.
    If still running, kill everything.

    futs = [concurrent.futures.Future, ...]
    exception = Exception
    """
    logging.info("Exiting")
    exit_event.set()

    time_of_exit = time.monotonic()

    if exception:
        logging.exception(
            'The following Exception was raised before the shutdown')
        try:
            raise exception
        except BaseException:
            tb = traceback.format_exc()
        logging.exception(tb)
        logging.exception(exception)

    while True:
        time.sleep(1)
        all_fut_exited = all([f.done() for f in futs])

        if time_of_exit + 40 < time.monotonic():
            logging.warning(
                f"Program still running after {time.monotonic()-time_of_exit}s, forcefully exiting.")
            os_exit(0)

        if all_fut_exited:
            logging.debug(
                f"Shutdown tooks {time.monotonic()-time_of_exit}s")
            logging.info('Exited. Cleaning up.')
            break

    sys_exit()


if __name__ == "__main__":

    current_path = utils.current_path()

    config_file = current_path / 'config.ini'

    config = configparser.ConfigParser()
    config.read(config_file)

    if bool(int(config['General']['DEBUG'])):
        logging.basicConfig(level=logging.DEBUG)
        logging.getLogger('imapclient').setLevel(logging.INFO)
        logging.debug('Enable debug logging')
    elif bool(int(config['General']['VERBOSE'])):
        logging.basicConfig(level=logging.INFO)
        logging.getLogger('imapclient').setLevel(logging.INFO)
        logging.info('Enable verbose logging')

    logging.debug(f"config_file: {config_file}")

    try:
        matrix_queue = queue.Queue(maxsize=5)
        exit_event = threading.Event()

        funcs = [main, send_to_matrix]

        executor = concurrent.futures.ThreadPoolExecutor(max_workers=2)
        futs = [executor.submit(f, matrix_queue, exit_event) for f in funcs]

        done, not_done = concurrent.futures.wait(
            futs, return_when=concurrent.futures.FIRST_EXCEPTION)

        if done:
            for fut in done:
                shutdown(futs, fut.exception())

    except KeyboardInterrupt:
        shutdown(futs)
