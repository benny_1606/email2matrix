import logging
from email import policy, utils
from email.parser import BytesParser
from datetime import timezone, timedelta


def fetch_email(server, config):

    new_msg = list()
    msg_uid_list = server.sort('ARRIVAL')
    logging.debug(f"msg_uid_list: {msg_uid_list}")
    msg_list = server.fetch(msg_uid_list, 'RFC822')

    bp = BytesParser(policy=policy.default)
    tz = timezone(timedelta(hours=int(config['General']['TIMEZONE'])))

    while True:
        try:
            uid, item = msg_list.popitem()
            msg = bp.parsebytes(item[b'RFC822'])

            msg_date = utils.parsedate_to_datetime(msg['Date'])\
                .astimezone(tz=tz)
            msg_date = utils.format_datetime(msg_date)
            msg_body = msg.get_body(preferencelist=('plain'))\
                .get_content().rstrip()

            new_msg.insert(
                0,
                '---\n'
                f"# {uid}\n"
                f"> {msg.get_all('Received')[2]}"
                '\n\n'
                f"`From`: {msg['From']}"
                '\n'
                f"> {msg_date}"
                '\n\n'
                f"`Subject`: {msg['Subject']}\n\n"
                '```'
                f"\n{msg_body}\n"
                '```'
            )

            logging.info(f"Email UID={uid} fetched")
        except KeyError:
            break

    status = server.delete_messages(msg_uid_list)
    logging.debug(f"delete_messages: {status}")

    status = server.expunge(msg_uid_list)
    logging.debug(f"expunge: {status}")

    return new_msg
